#!/bin/bash

oc create -f jenkins-slave-maven/buildconfig-template.yaml
oc new-app jenkins-slave-maven-builder

oc create -f jenkins-slave-go/buildconfig-template.yaml
oc new-app jenkins-slave-go-builder
